<?php

/*  Formato JSON esperado */

$arrEsperado = array();
$arrEmpresaEsperado = array();

$arrEsperado["peticion"] = "modificar";

$arrEmpresaEsperado["id_Empresa"] = "id (Un int)";
$arrEmpresaEsperado["Nombre"] = "Bugisos (Un string)";
$arrEmpresaEsperado["Tamanno"] = "tres Ases (Un int)";
$arrEmpresaEsperado["Director"] = "somos un utopia no necesitamos un lider, porque somos nuestros propios lideres (Una string)";
$arrEmpresaEsperado["Capital"] = "mucho dinero (Una int)";
$arrEmpresaEsperado["Pais"] = "espanita (Un string)";

$arrEsperado["empresaModificar"] = $arrEmpresaEsperado;


/* Funcion para comprobar si el recibido es igual al esperado */

function JSONCorrectoAnnadir($recibido){

	$auxCorrecto = false;

	if(isset($recibido["peticion"]) && $recibido["peticion"] ="modify" && isset($recibido["empresaModificar"])){

		$auxEmpresa = $recibido["empresaModificar"];
		if(isset($auxEmpresa["id_Empresa"]) && isset($auxEmpresa["Nombre"]) && isset($auxEmpresa["Tamanno"]) && isset($auxEmpresa["Pais"])
	 && isset($auxEmpresa["Capital"]) && isset($auxEmpresa["Director"]))
	 {
			$auxCorrecto = true;
		}

	}


	return $auxCorrecto;

}
