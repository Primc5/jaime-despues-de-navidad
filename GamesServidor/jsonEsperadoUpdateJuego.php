<?php

/*  Formato JSON esperado */

$arrEsperado = array();
$arrJuegoEsperado = array();

$arrEsperado["peticion"] = "modificar";

$arrJuegoEsperado["id"] = "id (Un int)";
$arrJuegoEsperado["tipo"] = "tres As (Un int)";
$arrJuegoEsperado["Empresa"] = "perro (Un string)";
$arrJuegoEsperado["Creacion"] = "2013-06-14 (Una date)";
$arrJuegoEsperado["nombre"] = "El ultimo de nosotros (Un string)";

$arrEsperado["juegoModificar"] = $arrJuegoEsperado;


/* Funcion para comprobar si el recibido es igual al esperado */

function JSONCorrectoAnnadir($recibido){

	$auxCorrecto = false;

	if(isset($recibido["peticion"]) && $recibido["peticion"] ="modify" && isset($recibido["juegoModificar"])){

		$auxJuego = $recibido["juegoModificar"];
		if(isset($auxJuego["id"]) && isset($auxJuego["nombre"])
		&& isset($auxJuego["tipo"]) && isset($auxJuego["Empresa"])
	  && isset($auxJuego["Creacion"]))
	 {
			$auxCorrecto = true;
		}

	}


	return $auxCorrecto;

}
