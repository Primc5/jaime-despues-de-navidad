
var id_borrar;

function borraHijos(elemento){

	var hijos = elemento.childNodes;

	//alert(hijos.length);
	for(var i=0; i < hijos.length ; i++){
		elemento.removeChild(hijos[i]);
	}

}

function ajustarJuego(elemento){

	id = elemento.id;

	pos = id.indexOf("_");

	tam = id.length

	idjugador = id.substring(pos+1,tam);

	nombre = document.getElementById("nombrejugador_" + idjugador).innerHTML;
	tipo = document.getElementById("tipojugador_" + idjugador).innerHTML;
	empresa = document.getElementById("empresajugador_" + idjugador).innerHTML;
	creacion = document.getElementById("creacionjugador_" + idjugador).innerHTML;


	document.getElementById("nombreJuego").value = nombre;
	document.getElementById("tipoJuego").value = tipo;
	document.getElementById("empresaJuego").value = empresa;
	document.getElementById("creacionJuego").value = creacion;

	id_borrar = idjugador;

  document.getElementById("btnBorrarJuego").disabled = false;
	document.getElementById("btnModificarJuego").disabled = false;
}

function ajustarEmpresa(elemento){

	id = elemento.id;

	pos = id.indexOf("_");

	tam = id.length

	idjugador = id.substring(pos+1,tam);

	nombre = document.getElementById("nombreEmpresa_" + idjugador).innerHTML;
	tamanno = document.getElementById("tamannoEmpresa_" + idjugador).innerHTML;
	pais = document.getElementById("paisEmpresa_" + idjugador).innerHTML;
	capital = document.getElementById("capitalEmpresa_" + idjugador).innerHTML;
	director = document.getElementById("directorEmpresa_" + idjugador).innerHTML;

	document.getElementById("nombreEmpresa").value = nombre;
	document.getElementById("tamañoEmpresa").value = tamanno;
	document.getElementById("paisEmpresa").value = pais;
	document.getElementById("capitalEmpresa").value = capital;
	document.getElementById("directorEmpresa").value = director;

	id_borrar = idjugador;

	document.getElementById("btnBorrarEmpresa").disabled = false;
	document.getElementById("btnModificarEmpresa").disabled = false;
}


//Funcion que llama a las cargas
function cambiaDos(){
	cargaEmpresas();
	cargaJuegos();

	}

//Funcion para cargar los juegos
function cargaJuegos(){

	var capa = document.getElementById("principal");

	borraHijos(capa);

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			configurarTablaJuegos(this.responseText);
		}else{
			console.log(this.readyState + " " + this.status);
		}
	};

	xhttp.open("GET", "../../jaime-despues-de-navidad/GamesServidor/leeJuegos.php", true);
	xhttp.send();


}
//Funcion para cargar las Empresas
function cargaEmpresas(){

	var capa = document.getElementById("principalEmpresa");

	borraHijos(capa);

	var xhttp2 = new XMLHttpRequest();

	xhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			configurarTablaEmpresas(this.responseText);
		}else{
			console.log(this.readyState + " " + this.status);
		}
	};

	xhttp2.open("GET", "../../jaime-despues-de-navidad/GamesServidor/leeEmpresas.php", true);
	xhttp2.send();


}

//Funcion para pintar los juegos
function pintaTablaJuegos(juegos){
	var capa = document.getElementById("principal");
	var fila = document.createElement("div");
	fila.setAttribute("id","jugador_"+ juegos.Id );
	fila.setAttribute("class","jugador");
	fila.setAttribute("onclick","ajustarJuego(this)");

	var nombre = document.createElement("h2");
	var texto = document.createTextNode(juegos.Nombre);
	nombre.appendChild(texto);
	nombre.setAttribute("id","nombrejugador_"+ juegos.Id );

	var tipo = document.createElement("h2");
	var textotipo = document.createTextNode(juegos.Tipo);
	tipo.appendChild(textotipo);
	tipo.setAttribute("id","tipojugador_"+ juegos.Id );

	var empresa = document.createElement("h2");
	var textoempresa = document.createTextNode(juegos.Empresa);
	empresa.appendChild(textoempresa);
	empresa.setAttribute("id","empresajugador_"+ juegos.Id );

	var creacion = document.createElement("h2");
	var textocreacion = document.createTextNode(juegos.Creacion);
	creacion.appendChild(textocreacion);
	creacion.setAttribute("id","creacionjugador_"+ juegos.Id );

	fila.appendChild(nombre);
	fila.appendChild(tipo);
	fila.appendChild(empresa);
	fila.appendChild(creacion);

	capa.appendChild(fila);
}


//Funcion para pintar las empresas
function pintaTablaEmpresas(empresas){
	var capa = document.getElementById("principalEmpresa");
	var fila = document.createElement("div");
	fila.setAttribute("id","jugador_"+ empresas.Id );
	fila.setAttribute("class","jugador");
	fila.setAttribute("onclick","ajustarEmpresa(this)");

	var nombre = document.createElement("h2");
	var texto = document.createTextNode(empresas.Nombre);
	nombre.appendChild(texto);
	nombre.setAttribute("id","nombreEmpresa_"+ empresas.Id );

	var tamanno = document.createElement("h2");
	var textotamanno = document.createTextNode(empresas.Tamanno);
	tamanno.appendChild(textotamanno);
	tamanno.setAttribute("id","tamannoEmpresa_"+ empresas.Id );

	var pais = document.createElement("h2");
	var textopais = document.createTextNode(empresas.Pais);
	pais.appendChild(textopais);
	pais.setAttribute("id","paisEmpresa_"+ empresas.Id );

	var capital = document.createElement("h2");
	var textocapital = document.createTextNode(empresas.Capital);
	capital.appendChild(textocapital);
	capital.setAttribute("id","capitalEmpresa_"+ empresas.Id );

  var director = document.createElement("h2");
	var textodirector = document.createTextNode(empresas.Director);
	director.appendChild(textodirector);
	director.setAttribute("id","directorEmpresa_"+ empresas.Id );

	fila.appendChild(nombre);
	fila.appendChild(tamanno);
	fila.appendChild(pais);
	fila.appendChild(capital);
  fila.appendChild(director);

	capa.appendChild(fila);
}

//Funcion para configurar los juegos
function configurarTablaJuegos(respuesta){

	var respuestaJSON = JSON.parse(respuesta);

	if(respuestaJSON["estado"] == "ok"){
		console.log("VAMOS BIEN");

		var arrJugadores =  respuestaJSON["juegos"];

		for(var i = 0; i < arrJugadores.length; i++){
			pintaTablaJuegos(arrJugadores[i]);
		}

	}else{
		console.log("VAMOS MAL");
	}
}


//Funcion para configurar las empresas
function configurarTablaEmpresas(respuesta){

	var respuestaJSON = JSON.parse(respuesta);

	if(respuestaJSON["estado"] == "ok"){
		console.log("VAMOS BIEN");

		var arrJugadores =  respuestaJSON["equipos"];

		for(var i = 0; i < arrJugadores.length; i++){
			pintaTablaEmpresas(arrJugadores[i]);
		}

	}else{
		console.log("VAMOS MAL");
	}
}

//Funcion para borrar un juego
function borrarJuego(){
		var boton = document.getElementById("btnBorrarJuego")
		boton.disabled = true;
		var juego = {};

		juego.Id = id_borrar;

		console.log(juego);
		var peticion = {};

		peticion.peticion = "delete";
		peticion.juegoBorrar = juego;

		console.log(peticion);

		peticionJSON = JSON.stringify(peticion);

		console.log(peticionJSON);

		var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
		xmlhttp.open("POST", "../../jaime-despues-de-navidad/GamesServidor/borrarJuegos.php");
		xmlhttp.setRequestHeader("Content-Type", "application/json");

		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				console.log(this.responseText);

				var respuestaJSON = JSON.parse(this.responseText);
				if(respuestaJSON["estado"] == "ok"){
					boton.disabled = false;
					alert("BORRADO CORRECTAMENTE");
				}else{
					alert(respuestaJSON["mensaje"]);
				}
			}else{
				console.log(this.readyState + " " + this.status);
				if (this.readyState == 4 && this.status == 404) {
					alert("URL INCORRECTA");
				}
			}
		};
		xmlhttp.send(peticionJSON);
}

//Funcion para borrar una Empresa
function borrarEmpresa(){

		var boton = document.getElementById("btnBorrarEmpresa")
		boton.disabled = true;
		var empresa = {};

		empresa.id_Empresa = id_borrar;

		console.log(empresa);
		var peticion = {};

		peticion.peticion = "delete";
		peticion.EmpresaBorrar = empresa;

		console.log(peticion);

		peticionJSON = JSON.stringify(peticion);

		console.log(peticionJSON);

		var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
		xmlhttp.open("POST", "../../jaime-despues-de-navidad/GamesServidor/borrarEmpresas.php");
		xmlhttp.setRequestHeader("Content-Type", "application/json");

		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				console.log(this.responseText);

				var respuestaJSON = JSON.parse(this.responseText);
				if(respuestaJSON["estado"] == "ok"){
					boton.disabled = false;
					alert("BORRADO CORRECTAMENTE");
				}else{
					alert(respuestaJSON["mensaje"]);
					console.log(respuestaJSON["error"]);
					console.log(respuestaJSON["query"]);
				}
			}else{
				console.log(this.readyState + " " + this.status);
				if (this.readyState == 4 && this.status == 404) {
					alert("URL INCORRECTA");
				}
			}
		};
		xmlhttp.send(peticionJSON);
}

//Funcion para modificar un juego
function modificarJuego(){
		var boton = document.getElementById("btnModificarJuego")
		boton.disabled = true;
		var juego = {};

		juego.nombre = document.getElementById("nombreJuego").value;
		juego.tipo = document.getElementById("tipoJuego").value;
		juego.Empresa = document.getElementById("empresaJuego").value;
		juego.Creacion = document.getElementById("creacionJuego").value;
		juego.id = id_borrar;

		console.log(juego);
		var peticion = {};

		peticion.peticion = "modify";
		peticion.juegoModificar = juego;

		console.log(peticion);

		peticionJSON = JSON.stringify(peticion);

		console.log(peticionJSON);

		var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
		xmlhttp.open("POST", "../../jaime-despues-de-navidad/GamesServidor/updateJuegos.php");
		xmlhttp.setRequestHeader("Content-Type", "application/json");

		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				console.log(this.responseText);

				var respuestaJSON = JSON.parse(this.responseText);
				if(respuestaJSON["estado"] == "ok"){
					boton.disabled = false;
					alert("MODIFICADO CORRECTAMENTE");
				}else{
					alert(respuestaJSON["mensaje"]);
				}
			}else{
				console.log(this.readyState + " " + this.status);
				if (this.readyState == 4 && this.status == 404) {
					alert("URL INCORRECTA");
				}
			}
		};
		xmlhttp.send(peticionJSON);
}

//Funcion para modificar una empresa
function modificarEmpresa(){
		var boton = document.getElementById("btnModificarEmpresa")
		boton.disabled = true;
		var empresa = {};

		empresa.Nombre = document.getElementById("nombreEmpresa").value;
		empresa.Tamanno = document.getElementById("tamañoEmpresa").value;
		empresa.Director = document.getElementById("directorEmpresa").value;
		empresa.Capital = document.getElementById("capitalEmpresa").value;
		empresa.Pais = document.getElementById("paisEmpresa").value;
		empresa.id_Empresa = id_borrar;

		console.log(empresa);
		var peticion = {};

		peticion.peticion = "modify";
		peticion.empresaModificar = empresa;

		console.log(peticion);

		peticionJSON = JSON.stringify(peticion);

		console.log(peticionJSON);

		var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
		xmlhttp.open("POST", "../../jaime-despues-de-navidad/GamesServidor/updateEmpresas.php");
		xmlhttp.setRequestHeader("Content-Type", "application/json");

		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				console.log(this.responseText);

				var respuestaJSON = JSON.parse(this.responseText);
				if(respuestaJSON["estado"] == "ok"){
					boton.disabled = false;
					alert("MODIFICADO CORRECTAMENTE");
				}else{
					alert(respuestaJSON["mensaje"]);
				}
			}else{
				console.log(this.readyState + " " + this.status);
				if (this.readyState == 4 && this.status == 404) {
					alert("URL INCORRECTA");
				}
			}
		};
		xmlhttp.send(peticionJSON);
}

//Funcion para insertar un juego
function insertarJuego(){
	var boton = document.getElementById("btnJuego")
	boton.disabled = true;
	var juego = {};

	juego.nombre = document.getElementById("nombreJuego").value;
	juego.tipo = document.getElementById("tipoJuego").value;
	juego.Empresa = document.getElementById("empresaJuego").value;
	juego.Creacion = document.getElementById("creacionJuego").value;

	console.log(juego);
	var peticion = {};

	peticion.peticion = "add";
	peticion.juegoAnnadir = juego;

	console.log(peticion);

	peticionJSON = JSON.stringify(peticion);

	console.log(peticionJSON);

	var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
	xmlhttp.open("POST", "../../jaime-despues-de-navidad/GamesServidor/escribirJuegos.php");
	xmlhttp.setRequestHeader("Content-Type", "application/json");

	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log(this.responseText);

			var respuestaJSON = JSON.parse(this.responseText);
			if(respuestaJSON["estado"] == "ok"){
				boton.disabled = false;
				pintaTablaJuegos(juego);
				alert("INSERTADO CORRECTAMENTE." );
			}else{
				alert(respuestaJSON["mensaje"]);
			}
		}else{
			console.log(this.readyState + " " + this.status);
			if (this.readyState == 4 && this.status == 404) {
				alert("URL INCORRECTA");
			}
		}
	};
	xmlhttp.send(peticionJSON);
}

//Funcion para insertar una Empresa
function insertarEmpresa(){
	var boton = document.getElementById("btnEmpresa")
	boton.disabled = true;
	var empresa = {};

	empresa.Nombre = document.getElementById("nombreEmpresa").value;
	empresa.Tamanno = document.getElementById("tamañoEmpresa").value;
	empresa.Director = document.getElementById("directorEmpresa").value;
	empresa.Capital = document.getElementById("capitalEmpresa").value;
	empresa.Pais = document.getElementById("paisEmpresa").value;

	console.log(empresa);
	var peticion = {};

	peticion.peticion = "add";
	peticion.empresaAnnadir = empresa;

	console.log(peticion);

	peticionJSON = JSON.stringify(peticion);

	console.log(peticionJSON);

	var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
	xmlhttp.open("POST", "../../jaime-despues-de-navidad/GamesServidor/escribirEmpresas.php");
	xmlhttp.setRequestHeader("Content-Type", "application/json");

	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log(this.responseText);
			var respuestaJSON = JSON.parse(this.responseText);
			if(respuestaJSON["estado"] == "ok"){
				boton.disabled = false;
				pintaTablaEmpresas(empresa);
				alert("INSERTADO CORRECTAMENTE." );
			}else{
				alert(respuestaJSON["mensaje"]);
			}
		}else{
			console.log(this.readyState + " " + this.status);
			if (this.readyState == 4 && this.status == 404) {
				alert("URL INCORRECTA");
			}
		}
	};
	xmlhttp.send(peticionJSON);
}
console.log("JS CARGADO");
